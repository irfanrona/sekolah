/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 100413
 Source Host           : localhost:3306
 Source Schema         : sekolah

 Target Server Type    : MySQL
 Target Server Version : 100413
 File Encoding         : 65001

 Date: 14/08/2022 16:22:07
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ad_spaces
-- ----------------------------
DROP TABLE IF EXISTS `ad_spaces`;
CREATE TABLE `ad_spaces`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ad_space` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ad_code_728` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ad_code_468` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ad_code_300` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ad_code_234` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ad_spaces
-- ----------------------------
INSERT INTO `ad_spaces` VALUES (1, 'index_top', NULL, NULL, NULL, NULL);
INSERT INTO `ad_spaces` VALUES (2, 'index_bottom', NULL, NULL, NULL, NULL);
INSERT INTO `ad_spaces` VALUES (3, 'post_top', NULL, NULL, NULL, NULL);
INSERT INTO `ad_spaces` VALUES (4, 'post_bottom', NULL, NULL, NULL, NULL);
INSERT INTO `ad_spaces` VALUES (5, 'category_top', NULL, NULL, NULL, NULL);
INSERT INTO `ad_spaces` VALUES (6, 'category_bottom', NULL, NULL, NULL, NULL);
INSERT INTO `ad_spaces` VALUES (7, 'tag_top', NULL, NULL, NULL, NULL);
INSERT INTO `ad_spaces` VALUES (8, 'tag_bottom', NULL, NULL, NULL, NULL);
INSERT INTO `ad_spaces` VALUES (9, 'search_top', NULL, NULL, NULL, NULL);
INSERT INTO `ad_spaces` VALUES (10, 'search_bottom', NULL, NULL, NULL, NULL);
INSERT INTO `ad_spaces` VALUES (11, 'profile_top', NULL, NULL, NULL, NULL);
INSERT INTO `ad_spaces` VALUES (12, 'profile_bottom', NULL, NULL, NULL, NULL);
INSERT INTO `ad_spaces` VALUES (13, 'reading_list_top', NULL, NULL, NULL, NULL);
INSERT INTO `ad_spaces` VALUES (14, 'reading_list_bottom', NULL, NULL, NULL, NULL);
INSERT INTO `ad_spaces` VALUES (15, 'sidebar_top', NULL, NULL, NULL, NULL);
INSERT INTO `ad_spaces` VALUES (16, 'sidebar_bottom', NULL, NULL, NULL, NULL);
INSERT INTO `ad_spaces` VALUES (17, 'header', NULL, NULL, NULL, NULL);
INSERT INTO `ad_spaces` VALUES (18, 'posts_top', NULL, NULL, NULL, NULL);
INSERT INTO `ad_spaces` VALUES (19, 'posts_bottom', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for audios
-- ----------------------------
DROP TABLE IF EXISTS `audios`;
CREATE TABLE `audios`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `audio_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `audio_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `download_button` tinyint(1) NULL DEFAULT 1,
  `user_id` int(11) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_id` int(11) NULL DEFAULT 1,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name_slug` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `parent_id` int(11) NULL DEFAULT 0,
  `description` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `keywords` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `color` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `block_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `category_order` int(11) NULL DEFAULT 0,
  `show_at_homepage` tinyint(1) NULL DEFAULT 1,
  `show_on_menu` tinyint(1) NULL DEFAULT 1,
  `created_at` timestamp(0) NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES (2, 2, 'Ekstrakurikuler', 'eskul', 0, '', '', '#d8891b', 'block-3', 5, 1, 1, '2022-08-14 15:15:33');
INSERT INTO `categories` VALUES (3, 2, 'Prestasi', 'prestasi', 0, '', '', '#d54b4b', 'block-1', 4, 1, 1, '2022-08-14 15:16:38');
INSERT INTO `categories` VALUES (4, 2, 'Kegiatan', 'kegiatan', 0, '', '', '#2440a0', 'block-1', 3, 1, 1, '2022-08-14 15:46:42');

-- ----------------------------
-- Table structure for ci_sessions
-- ----------------------------
DROP TABLE IF EXISTS `ci_sessions`;
CREATE TABLE `ci_sessions`  (
  `id` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ip_address` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `data` blob NOT NULL,
  INDEX `ci_sessions_timestamp`(`timestamp`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ci_sessions
-- ----------------------------
INSERT INTO `ci_sessions` VALUES ('7kh6bjarmui661mjfri4e36roqj97kpd', '::1', 1612781634, 0x5F5F63695F6C6173745F726567656E65726174657C693A313631323738313633343B76725F736573735F757365725F69647C733A313A2231223B76725F736573735F757365725F656D61696C7C733A32383A22697266616E2E726F6E6139354073747564656E742E7570692E656475223B76725F736573735F757365725F726F6C657C733A353A2261646D696E223B76725F736573735F6C6F676765645F696E7C623A313B76725F736573735F6170705F6B65797C733A36303A22647A6964746235327362677064707A79786C63626C38397178656E343733656279653274756876776D6D6964756C6F6E627837703532307462767030223B);
INSERT INTO `ci_sessions` VALUES ('q9muu7m5rkhr3d80tk7p5bdbv3bu2tnb', '::1', 1612781957, 0x5F5F63695F6C6173745F726567656E65726174657C693A313631323738313738343B76725F736573735F757365725F69647C733A313A2232223B76725F736573735F757365725F656D61696C7C733A32303A22697266616E726F6E613340676D61696C2E636F6D223B76725F736573735F757365725F726F6C657C733A393A226D6F64657261746F72223B76725F736573735F6C6F676765645F696E7C623A313B76725F736573735F6170705F6B65797C733A36303A22647A6964746235327362677064707A79786C63626C38397178656E343733656279653274756876776D6D6964756C6F6E627837703532307462767030223B);
INSERT INTO `ci_sessions` VALUES ('d70smik088m8h4470cr1bkdtmdkdehvq', '::1', 1614173574, 0x5F5F63695F6C6173745F726567656E65726174657C693A313631343137333533323B76725F736573735F757365725F69647C733A313A2231223B76725F736573735F757365725F656D61696C7C733A32383A22697266616E2E726F6E6139354073747564656E742E7570692E656475223B76725F736573735F757365725F726F6C657C733A353A2261646D696E223B76725F736573735F6C6F676765645F696E7C623A313B76725F736573735F6170705F6B65797C733A36303A22647A6964746235327362677064707A79786C63626C38397178656E343733656279653274756876776D6D6964756C6F6E627837703532307462767030223B);
INSERT INTO `ci_sessions` VALUES ('hm24tvied2ie3k9f2nu2susd2ao38i1i', '::1', 1638950235, 0x5F5F63695F6C6173745F726567656E65726174657C693A313633383934393937343B76725F736573735F757365725F69647C733A313A2231223B76725F736573735F757365725F656D61696C7C733A32383A22697266616E2E726F6E6139354073747564656E742E7570692E656475223B76725F736573735F757365725F726F6C657C733A353A2261646D696E223B76725F736573735F6C6F676765645F696E7C623A313B76725F736573735F6170705F6B65797C733A36303A22647A6964746235327362677064707A79786C63626C38397178656E343733656279653274756876776D6D6964756C6F6E627837703532307462767030223B);
INSERT INTO `ci_sessions` VALUES ('qnmq0jrksh175fstq4djd50837pq5hn1', '::1', 1660468866, 0x5F5F63695F6C6173745F726567656E65726174657C693A313636303436383835313B76725F736573735F757365725F69647C733A313A2231223B76725F736573735F757365725F656D61696C7C733A32383A22697266616E2E726F6E6139354073747564656E742E7570692E656475223B76725F736573735F757365725F726F6C657C733A353A2261646D696E223B76725F736573735F6C6F676765645F696E7C623A313B76725F736573735F6170705F6B65797C733A36303A22647A6964746235327362677064707A79786C63626C38397178656E343733656279653274756876776D6D6964756C6F6E627837703532307462767030223B);

-- ----------------------------
-- Table structure for comments
-- ----------------------------
DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NULL DEFAULT 0,
  `post_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `comment` varchar(5000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ip_address` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `like_count` int(11) NULL DEFAULT 0,
  `status` tinyint(1) NULL DEFAULT 1,
  `created_at` timestamp(0) NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_parent_id`(`parent_id`) USING BTREE,
  INDEX `idx_post_id`(`post_id`) USING BTREE,
  INDEX `idx_status`(`status`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for contacts
-- ----------------------------
DROP TABLE IF EXISTS `contacts`;
CREATE TABLE `contacts`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `message` varchar(5000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for files
-- ----------------------------
DROP TABLE IF EXISTS `files`;
CREATE TABLE `files`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `file_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for followers
-- ----------------------------
DROP TABLE IF EXISTS `followers`;
CREATE TABLE `followers`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `following_id` int(11) NULL DEFAULT NULL,
  `follower_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for fonts
-- ----------------------------
DROP TABLE IF EXISTS `fonts`;
CREATE TABLE `fonts`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `font_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `font_url` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `font_family` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `is_default` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fonts
-- ----------------------------
INSERT INTO `fonts` VALUES (1, 'Arial', NULL, 'font-family: Arial, Helvetica, sans-serif', 1);
INSERT INTO `fonts` VALUES (2, 'Arvo', '<link href=\"https://fonts.googleapis.com/css?family=Arvo:400,700&display=swap\" rel=\"stylesheet\">\r\n', 'font-family: \"Arvo\", Helvetica, sans-serif', 0);
INSERT INTO `fonts` VALUES (3, 'Averia Libre', '<link href=\"https://fonts.googleapis.com/css?family=Averia+Libre:300,400,700&display=swap\" rel=\"stylesheet\">\r\n', 'font-family: \"Averia Libre\", Helvetica, sans-serif', 0);
INSERT INTO `fonts` VALUES (4, 'Bitter', '<link href=\"https://fonts.googleapis.com/css?family=Bitter:400,400i,700&display=swap&subset=latin-ext\" rel=\"stylesheet\">\r\n', 'font-family: \"Bitter\", Helvetica, sans-serif', 0);
INSERT INTO `fonts` VALUES (5, 'Cabin', '<link href=\"https://fonts.googleapis.com/css?family=Cabin:400,500,600,700&display=swap&subset=latin-ext,vietnamese\" rel=\"stylesheet\">\r\n', 'font-family: \"Cabin\", Helvetica, sans-serif', 0);
INSERT INTO `fonts` VALUES (6, 'Cherry Swash', '<link href=\"https://fonts.googleapis.com/css?family=Cherry+Swash:400,700&display=swap&subset=latin-ext\" rel=\"stylesheet\">\r\n', 'font-family: \"Cherry Swash\", Helvetica, sans-serif', 0);
INSERT INTO `fonts` VALUES (7, 'Encode Sans', '<link href=\"https://fonts.googleapis.com/css?family=Encode+Sans:300,400,500,600,700&display=swap&subset=latin-ext,vietnamese\" rel=\"stylesheet\">\r\n', 'font-family: \"Encode Sans\", Helvetica, sans-serif', 0);
INSERT INTO `fonts` VALUES (8, 'Helvetica', NULL, 'font-family: Helvetica, sans-serif', 1);
INSERT INTO `fonts` VALUES (9, 'Hind', '<link href=\"https://fonts.googleapis.com/css?family=Hind:300,400,500,600,700&display=swap&subset=devanagari,latin-ext\" rel=\"stylesheet\">', 'font-family: \"Hind\", Helvetica, sans-serif', 0);
INSERT INTO `fonts` VALUES (10, 'Josefin Sans', '<link href=\"https://fonts.googleapis.com/css?family=Josefin+Sans:300,400,600,700&display=swap&subset=latin-ext,vietnamese\" rel=\"stylesheet\">\r\n', 'font-family: \"Josefin Sans\", Helvetica, sans-serif', 0);
INSERT INTO `fonts` VALUES (11, 'Kalam', '<link href=\"https://fonts.googleapis.com/css?family=Kalam:300,400,700&display=swap&subset=devanagari,latin-ext\" rel=\"stylesheet\">\r\n', 'font-family: \"Kalam\", Helvetica, sans-serif', 0);
INSERT INTO `fonts` VALUES (12, 'Khula', '<link href=\"https://fonts.googleapis.com/css?family=Khula:300,400,600,700&display=swap&subset=devanagari,latin-ext\" rel=\"stylesheet\">\r\n', 'font-family: \"Khula\", Helvetica, sans-serif', 0);
INSERT INTO `fonts` VALUES (13, 'Lato', '<link href=\"https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap&subset=latin-ext\" rel=\"stylesheet\">', 'font-family: \"Lato\", Helvetica, sans-serif', 0);
INSERT INTO `fonts` VALUES (14, 'Lora', '<link href=\"https://fonts.googleapis.com/css?family=Lora:400,700&display=swap&subset=cyrillic,cyrillic-ext,latin-ext,vietnamese\" rel=\"stylesheet\">\r\n', 'font-family: \"Lora\", Helvetica, sans-serif', 0);
INSERT INTO `fonts` VALUES (15, 'Merriweather', '<link href=\"https://fonts.googleapis.com/css?family=Merriweather:300,400,700&display=swap&subset=cyrillic,cyrillic-ext,latin-ext,vietnamese\" rel=\"stylesheet\">\r\n', 'font-family: \"Merriweather\", Helvetica, sans-serif', 0);
INSERT INTO `fonts` VALUES (16, 'Montserrat', '<link href=\"https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700&display=swap&subset=cyrillic,cyrillic-ext,latin-ext,vietnamese\" rel=\"stylesheet\">\r\n', 'font-family: \"Montserrat\", Helvetica, sans-serif', 0);
INSERT INTO `fonts` VALUES (17, 'Mukta', '<link href=\"https://fonts.googleapis.com/css?family=Mukta:300,400,500,600,700&display=swap&subset=devanagari,latin-ext\" rel=\"stylesheet\">\r\n', 'font-family: \"Mukta\", Helvetica, sans-serif', 0);
INSERT INTO `fonts` VALUES (18, 'Nunito', '<link href=\"https://fonts.googleapis.com/css?family=Nunito:300,400,600,700&display=swap&subset=cyrillic,cyrillic-ext,latin-ext,vietnamese\" rel=\"stylesheet\">\r\n', 'font-family: \"Nunito\", Helvetica, sans-serif', 0);
INSERT INTO `fonts` VALUES (19, 'Open Sans', '<link href=\"https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese\" rel=\"stylesheet\">', 'font-family: \"Open Sans\", Helvetica, sans-serif', 0);
INSERT INTO `fonts` VALUES (20, 'Oswald', '<link href=\"https://fonts.googleapis.com/css?family=Oswald:300,400,500,600,700&display=swap&subset=cyrillic,cyrillic-ext,latin-ext,vietnamese\" rel=\"stylesheet\">', 'font-family: \"Oswald\", Helvetica, sans-serif', 0);
INSERT INTO `fonts` VALUES (21, 'Oxygen', '<link href=\"https://fonts.googleapis.com/css?family=Oxygen:300,400,700&display=swap&subset=latin-ext\" rel=\"stylesheet\">\r\n', 'font-family: \"Oxygen\", Helvetica, sans-serif', 0);
INSERT INTO `fonts` VALUES (22, 'Poppins', '<link href=\"https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap&subset=devanagari,latin-ext\" rel=\"stylesheet\">\r\n', 'font-family: \"Poppins\", Helvetica, sans-serif', 0);
INSERT INTO `fonts` VALUES (23, 'PT Sans', '<link href=\"https://fonts.googleapis.com/css?family=PT+Sans:400,700&display=swap&subset=cyrillic,cyrillic-ext,latin-ext\" rel=\"stylesheet\">\r\n', 'font-family: \"PT Sans\", Helvetica, sans-serif', 0);
INSERT INTO `fonts` VALUES (24, 'Raleway', '<link href=\"https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700&display=swap&subset=latin-ext\" rel=\"stylesheet\">\r\n', 'font-family: \"Raleway\", Helvetica, sans-serif', 0);
INSERT INTO `fonts` VALUES (25, 'Roboto', '<link href=\"https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese\" rel=\"stylesheet\">', 'font-family: \"Roboto\", Helvetica, sans-serif', 0);
INSERT INTO `fonts` VALUES (26, 'Roboto Condensed', '<link href=\"https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700&display=swap&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese\" rel=\"stylesheet\">\r\n', 'font-family: \"Roboto Condensed\", Helvetica, sans-serif', 0);
INSERT INTO `fonts` VALUES (27, 'Roboto Slab', '<link href=\"https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,500,600,700&display=swap&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese\" rel=\"stylesheet\">\r\n', 'font-family: \"Roboto Slab\", Helvetica, sans-serif', 0);
INSERT INTO `fonts` VALUES (28, 'Rokkitt', '<link href=\"https://fonts.googleapis.com/css?family=Rokkitt:300,400,500,600,700&display=swap&subset=latin-ext,vietnamese\" rel=\"stylesheet\">\r\n', 'font-family: \"Rokkitt\", Helvetica, sans-serif', 0);
INSERT INTO `fonts` VALUES (29, 'Source Sans Pro', '<link href=\"https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700&display=swap&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese\" rel=\"stylesheet\">', 'font-family: \"Source Sans Pro\", Helvetica, sans-serif', 0);
INSERT INTO `fonts` VALUES (30, 'Titillium Web', '<link href=\"https://fonts.googleapis.com/css?family=Titillium+Web:300,400,600,700&display=swap&subset=latin-ext\" rel=\"stylesheet\">', 'font-family: \"Titillium Web\", Helvetica, sans-serif', 0);
INSERT INTO `fonts` VALUES (31, 'Ubuntu', '<link href=\"https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700&display=swap&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext\" rel=\"stylesheet\">', 'font-family: \"Ubuntu\", Helvetica, sans-serif', 0);
INSERT INTO `fonts` VALUES (32, 'Verdana', NULL, 'font-family: Verdana, Helvetica, sans-serif', 1);

-- ----------------------------
-- Table structure for gallery
-- ----------------------------
DROP TABLE IF EXISTS `gallery`;
CREATE TABLE `gallery`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_id` int(11) NULL DEFAULT 1,
  `title` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `album_id` int(11) NULL DEFAULT NULL,
  `category_id` int(11) NULL DEFAULT NULL,
  `path_big` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `path_small` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `is_album_cover` tinyint(1) NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for gallery_albums
-- ----------------------------
DROP TABLE IF EXISTS `gallery_albums`;
CREATE TABLE `gallery_albums`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_id` int(11) NULL DEFAULT 1,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for gallery_categories
-- ----------------------------
DROP TABLE IF EXISTS `gallery_categories`;
CREATE TABLE `gallery_categories`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_id` int(11) NULL DEFAULT 1,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `album_id` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for general_settings
-- ----------------------------
DROP TABLE IF EXISTS `general_settings`;
CREATE TABLE `general_settings`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_lang` int(11) NOT NULL DEFAULT 1,
  `multilingual_system` tinyint(1) NULL DEFAULT 1,
  `show_hits` tinyint(1) NULL DEFAULT 1,
  `show_rss` tinyint(1) NULL DEFAULT 1,
  `show_newsticker` tinyint(1) NULL DEFAULT 1,
  `pagination_per_page` smallint(6) NULL DEFAULT 10,
  `google_analytics` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `mail_library` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'swift',
  `mail_protocol` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'smtp',
  `mail_host` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `mail_port` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '587',
  `mail_username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `mail_password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `mail_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `google_client_id` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `google_client_secret` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `vk_app_id` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `vk_secure_key` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `facebook_app_id` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `facebook_app_secret` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `facebook_comment` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `facebook_comment_active` tinyint(1) NULL DEFAULT 1,
  `show_featured_section` tinyint(1) NULL DEFAULT 1,
  `show_latest_posts` tinyint(1) NULL DEFAULT 1,
  `registration_system` tinyint(1) NULL DEFAULT 1,
  `comment_system` tinyint(1) NULL DEFAULT 1,
  `comment_approval_system` tinyint(1) NULL DEFAULT 1,
  `show_post_author` tinyint(1) NULL DEFAULT 1,
  `show_post_date` tinyint(1) NULL DEFAULT 1,
  `menu_limit` tinyint(4) NULL DEFAULT 8,
  `custom_css_codes` mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `custom_javascript_codes` mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `adsense_activation_code` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `vr_key` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `purchase_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `recaptcha_site_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `recaptcha_secret_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `recaptcha_lang` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `emoji_reactions` tinyint(1) NULL DEFAULT 1,
  `mail_contact_status` tinyint(1) NULL DEFAULT 0,
  `mail_contact` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cache_system` tinyint(1) NULL DEFAULT 0,
  `cache_refresh_time` int(11) NULL DEFAULT 1800,
  `refresh_cache_database_changes` tinyint(1) NULL DEFAULT 0,
  `email_verification` tinyint(1) NULL DEFAULT 0,
  `file_manager_show_files` tinyint(1) NULL DEFAULT 1,
  `audio_download_button` tinyint(1) NULL DEFAULT 1,
  `approve_added_user_posts` tinyint(1) NULL DEFAULT 1,
  `approve_updated_user_posts` tinyint(1) NULL DEFAULT 1,
  `timezone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'America/New_York',
  `sort_slider_posts` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'by_slider_order',
  `sort_featured_posts` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'by_featured_order',
  `newsletter` tinyint(1) NULL DEFAULT 1,
  `text_editor_lang` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'en',
  `show_home_link` tinyint(1) NULL DEFAULT 1,
  `post_format_article` tinyint(1) NULL DEFAULT 1,
  `post_format_gallery` tinyint(1) NULL DEFAULT 1,
  `post_format_sorted_list` tinyint(1) NULL DEFAULT 1,
  `post_format_video` tinyint(1) NULL DEFAULT 1,
  `post_format_audio` tinyint(1) NULL DEFAULT 1,
  `post_format_trivia_quiz` tinyint(1) NULL DEFAULT 1,
  `post_format_personality_quiz` tinyint(1) NULL DEFAULT 1,
  `maintenance_mode_title` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'Coming Soon!',
  `maintenance_mode_description` varchar(5000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `maintenance_mode_status` tinyint(1) NULL DEFAULT 0,
  `sitemap_frequency` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'monthly',
  `sitemap_last_modification` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'server_response',
  `sitemap_priority` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'automatically',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of general_settings
-- ----------------------------
INSERT INTO `general_settings` VALUES (1, 2, 1, 1, 1, 1, 16, NULL, 'swift', 'smtp', NULL, '587', NULL, NULL, 'SMAN 3 Banjar', NULL, NULL, NULL, NULL, NULL, NULL, '', 0, 1, 1, 1, 1, 1, 1, 1, 9, '', '', NULL, 'wqweqwe', '', NULL, NULL, 'en', 0, 0, NULL, 0, 180000, 0, 1, 1, 1, 1, 1, 'Asia/Jakarta', 'by_slider_order', 'by_featured_order', 1, 'en', 1, 1, 1, 1, 1, 1, 1, 1, 'Coming Soon!', 'Our website is under construction. We\'ll be here soon with our new awesome site.', 0, 'weekly', 'server_response', 'automatically');

-- ----------------------------
-- Table structure for images
-- ----------------------------
DROP TABLE IF EXISTS `images`;
CREATE TABLE `images`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_id` int(11) NULL DEFAULT 1,
  `image_big` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `image_default` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `image_slider` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `image_mid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `image_small` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `image_mime` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'jpg',
  `file_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for languages
-- ----------------------------
DROP TABLE IF EXISTS `languages`;
CREATE TABLE `languages`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `short_form` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `language_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `folder_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `text_direction` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `language_order` smallint(6) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of languages
-- ----------------------------
INSERT INTO `languages` VALUES (1, 'English', 'en', 'en-US', 'default', 'ltr', 1, 1);
INSERT INTO `languages` VALUES (2, 'Indonesia', 'id', 'id', 'indonesia', 'ltr', 1, 1);

-- ----------------------------
-- Table structure for pages
-- ----------------------------
DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_id` int(11) NULL DEFAULT 1,
  `title` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `slug` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `keywords` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `is_custom` tinyint(1) NULL DEFAULT 1,
  `page_default_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `page_content` mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `page_order` smallint(6) NULL DEFAULT 1,
  `visibility` tinyint(1) NULL DEFAULT 1,
  `title_active` tinyint(1) NULL DEFAULT 1,
  `breadcrumb_active` tinyint(1) NULL DEFAULT 1,
  `right_column_active` tinyint(1) NULL DEFAULT 1,
  `need_auth` tinyint(1) NULL DEFAULT 0,
  `location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'top',
  `link` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `parent_id` int(11) NULL DEFAULT 0,
  `page_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'page',
  `created_at` timestamp(0) NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pages
-- ----------------------------
INSERT INTO `pages` VALUES (1, 1, 'Contact', 'contact', 'SMAN 3 Banjar Contact Page', 'SMAN 3 Banjar, contact, page', 0, 'contact', '', 99, 1, 1, 1, 0, 0, 'main', NULL, 0, 'page', '2020-02-18 18:09:21');
INSERT INTO `pages` VALUES (2, 1, 'Gallery', 'gallery', 'SMAN 3 Banjar Gallery Page', 'SMAN 3 Banjar, gallery, page', 0, 'gallery', NULL, 3, 1, 1, 1, 0, 0, 'main', NULL, 0, 'page', '2020-02-18 18:11:40');
INSERT INTO `pages` VALUES (3, 1, 'Terms & Condition', 'terms-conditions', 'Halaman Syarat & Ketentuan SMAN 3 Banjar', 'Terms, Conditions, SMAN18', 0, 'terms_conditions', '', 1, 1, 1, 1, 0, 0, 'footer', NULL, 0, 'page', '2020-02-18 18:12:40');
INSERT INTO `pages` VALUES (4, 2, 'Galeri', 'gallery', 'SMAN 3 Banjar Gallery Page', 'SMAN 3 Banjar, gallery , page', 0, 'gallery', '', 6, 1, 1, 1, 0, 0, 'main', '', 0, 'page', '2021-02-08 16:24:10');
INSERT INTO `pages` VALUES (5, 2, 'Kontak', 'contact', 'SMAN 3 Banjar Contact Page', 'SMAN 3 Banjar, contact, page', 0, 'contact', '', 7, 1, 1, 1, 0, 0, 'main', '', 0, 'page', '2021-02-08 16:24:10');
INSERT INTO `pages` VALUES (6, 2, 'Syarat & Ketentuan', 'terms-conditions', 'SMAN 3 Banjar Terms Conditions Page', 'SMAN 3 Banjar, terms, conditions', 0, 'terms_conditions', '', 1, 1, 1, 1, 0, 0, 'footer', '', 0, 'page', '2021-02-08 16:24:10');
INSERT INTO `pages` VALUES (8, 2, 'Tentang Sekolah', 'tentang', '', '', 1, NULL, '<p>Profil Sekolah</p>', 1, 1, 1, 1, 1, 0, 'main', NULL, 20, 'page', '2021-02-08 17:01:42');
INSERT INTO `pages` VALUES (10, 1, 'Profil Umum Sekolah BPI', 'profil-umum-sekolah-bpi', NULL, NULL, 1, NULL, NULL, 1, 1, 1, 1, 1, 0, 'main', 'profile', 8, 'link', '2021-02-08 17:09:38');
INSERT INTO `pages` VALUES (13, 2, 'PPDB SMAN 3 Banjar', 'ppdb-sman-3-banjar', '', '', 1, NULL, '<h1>PPDB ONLINE</h1>\r\n<ol>\r\n<li>Harus ganteng</li>\r\n<li>nilai raport</li>\r\n</ol>', 1, 1, 1, 1, 1, 0, 'none', NULL, 0, 'page', '2021-02-24 20:30:18');
INSERT INTO `pages` VALUES (20, 2, 'Profil', 'profil', NULL, NULL, 1, NULL, NULL, 2, 1, 1, 1, 1, 0, 'main', '#', 0, 'link', '2022-08-14 14:49:58');
INSERT INTO `pages` VALUES (21, 2, 'Visi Misi', 'visi-misi', '', '', 1, NULL, '<p>Ini Visi Misi</p>', 2, 1, 1, 1, 1, 0, 'none', NULL, 20, 'page', '2022-08-14 14:51:29');
INSERT INTO `pages` VALUES (22, 2, 'Kepala Sekolah', 'kepala-sekolah', '', '', 1, NULL, '<p style=\"text-align: center;\"><span><img src=\"https://i.ytimg.com/vi/kz0vqPtOHiw/maxresdefault.jpg\" width=\"1280\" height=\"720\" alt=\"\" /></span></p>\r\n<p style=\"text-align: center;\"><span></span></p>\r\n<p style=\"text-align: center;\"><span>ASSALAMU&rsquo;ALAIKUM WR. WB.</span></p>\r\n<p style=\"text-align: justify;\"><span>DENGAN MEMANJATKAN PUJI DAN SYUKUR KEHADIRAT ILLAHI RABBI, KITA SELALU ADA DALAM LINDUNGAN-NYA, SHOLAWAT SERTA SALAM SEMOGA DILIMPAHKAN KEPADA NABI AGUNG PEMBAWA RAHMAT NABI MUHAMMAD SAW.&nbsp;<br />MUDAH-MUDAHAN KITA MENDAPAT SAFAAT DI YAUMIL AKHIR.</span></p>\r\n<p style=\"text-align: justify;\"><span>PESAN UNTUK PESERTA DIDIK:<br />&nbsp; &nbsp; &nbsp;BANYAK SUKA DAN DUKA SELAMA MENUNTUT ILMU DI SMAN 3 BANJAR, JADIKAN SEMUA ITU SEBAGAI PENGALAMAN UNTUK MERAIH KESUKSESAN DIMASA DEPAN ANAK-ANAKKU.</span></p>\r\n<p style=\"text-align: justify;\"><span>SEGALA APA YANG DIBERIKAN OLEH GURU, ADALAH MODAL BAGIMU DIMASA DEPAN, SEMOGA NANDA SEMUA DAPAT MENERIMA DENGAN HATI YANG IKHLAS.<br />&nbsp; &nbsp; &nbsp;MAAFKAN KAMI SEMUA DAN JADILAH KAU PENERUS BANGSA YANG HANDAL DAN MENDAPAT RIDHO ALLOH SWT.<br />KAPANPUN, DIMANAPUN SAMPAIKANLAH INFORMASI UNTUK SMAN 3 BANJAR.<br />&nbsp; &nbsp; &nbsp;MOHON DO&rsquo;A SEMOGA SEKOLAH YANG NANDA UNTUK MENUNTUT ILMU MENJADI SEKOLAH YANG SUKSES DAN PENUH BAROKAH.</span></p>\r\n<p style=\"text-align: justify;\"><span>MUDAH-MUDAHKAN DENGAN MEDIA WEBSITE SMAN3 BANJAR DAPAT MEMPERLUAS INFORMASI KEBERADAAN SMAN 3 BANJAR.</span></p>\r\n<p style=\"text-align: justify;\"><span>AAMIIN.</span></p>', 3, 1, 1, 1, 1, 0, 'none', NULL, 20, 'page', '2022-08-14 14:51:46');

-- ----------------------------
-- Table structure for poll_votes
-- ----------------------------
DROP TABLE IF EXISTS `poll_votes`;
CREATE TABLE `poll_votes`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poll_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `vote` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for polls
-- ----------------------------
DROP TABLE IF EXISTS `polls`;
CREATE TABLE `polls`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_id` int(11) NULL DEFAULT 1,
  `question` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `option1` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `option2` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `option3` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `option4` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `option5` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `option6` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `option7` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `option8` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `option9` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `option10` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `status` tinyint(1) NULL DEFAULT 1,
  `vote_permission` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'all',
  `created_at` timestamp(0) NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for post_audios
-- ----------------------------
DROP TABLE IF EXISTS `post_audios`;
CREATE TABLE `post_audios`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NULL DEFAULT NULL,
  `audio_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for post_files
-- ----------------------------
DROP TABLE IF EXISTS `post_files`;
CREATE TABLE `post_files`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NULL DEFAULT NULL,
  `file_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for post_gallery_items
-- ----------------------------
DROP TABLE IF EXISTS `post_gallery_items`;
CREATE TABLE `post_gallery_items`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NULL DEFAULT NULL,
  `title` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `image_large` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `image_description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `item_order` smallint(6) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for post_images
-- ----------------------------
DROP TABLE IF EXISTS `post_images`;
CREATE TABLE `post_images`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NULL DEFAULT NULL,
  `image_big` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `image_default` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for post_pageviews
-- ----------------------------
DROP TABLE IF EXISTS `post_pageviews`;
CREATE TABLE `post_pageviews`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NULL DEFAULT NULL,
  `ip_address` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_post_id`(`post_id`) USING BTREE,
  INDEX `idx_created_at`(`created_at`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for post_sorted_list_items
-- ----------------------------
DROP TABLE IF EXISTS `post_sorted_list_items`;
CREATE TABLE `post_sorted_list_items`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NULL DEFAULT NULL,
  `title` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `image_large` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `image_description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `item_order` smallint(6) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for posts
-- ----------------------------
DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_id` int(11) NULL DEFAULT 1,
  `title` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `title_slug` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `title_hash` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `keywords` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `summary` varchar(5000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `category_id` int(11) NULL DEFAULT NULL,
  `image_big` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `image_default` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `image_slider` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `image_mid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `image_small` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `image_mime` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'jpg',
  `optional_url` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pageviews` int(11) NULL DEFAULT 0,
  `need_auth` tinyint(1) NULL DEFAULT 0,
  `is_slider` tinyint(1) NULL DEFAULT 0,
  `slider_order` tinyint(1) NULL DEFAULT 1,
  `is_featured` tinyint(1) NULL DEFAULT 0,
  `featured_order` tinyint(1) NULL DEFAULT 1,
  `is_recommended` tinyint(1) NULL DEFAULT 0,
  `is_breaking` tinyint(1) NULL DEFAULT 1,
  `is_scheduled` tinyint(1) NULL DEFAULT 0,
  `visibility` tinyint(1) NULL DEFAULT 1,
  `show_right_column` tinyint(1) NULL DEFAULT 1,
  `post_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'post',
  `video_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `image_url` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `video_url` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `video_embed_code` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT 1,
  `feed_id` int(11) NULL DEFAULT NULL,
  `post_url` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `show_post_url` tinyint(1) NULL DEFAULT 1,
  `image_description` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `show_item_numbers` tinyint(1) NULL DEFAULT 1,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_category_id`(`category_id`) USING BTREE,
  INDEX `idx_is_slider`(`is_slider`) USING BTREE,
  INDEX `idx_is_featured`(`is_featured`) USING BTREE,
  INDEX `idx_is_recommended`(`is_recommended`) USING BTREE,
  INDEX `idx_is_breaking`(`is_breaking`) USING BTREE,
  INDEX `idx_created_at`(`created_at`) USING BTREE,
  INDEX `idx_lang_id`(`lang_id`) USING BTREE,
  INDEX `idx_is_scheduled`(`is_scheduled`) USING BTREE,
  INDEX `idx_visibility`(`visibility`) USING BTREE,
  INDEX `idx_user_id`(`user_id`) USING BTREE,
  INDEX `idx_status`(`status`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for quiz_answers
-- ----------------------------
DROP TABLE IF EXISTS `quiz_answers`;
CREATE TABLE `quiz_answers`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NULL DEFAULT NULL,
  `image_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `answer_text` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `is_correct` tinyint(1) NULL DEFAULT NULL,
  `assigned_result_id` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for quiz_images
-- ----------------------------
DROP TABLE IF EXISTS `quiz_images`;
CREATE TABLE `quiz_images`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_default` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `image_small` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `file_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `image_mime` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'jpg',
  `user_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for quiz_questions
-- ----------------------------
DROP TABLE IF EXISTS `quiz_questions`;
CREATE TABLE `quiz_questions`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NULL DEFAULT NULL,
  `question` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `image_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `question_order` int(11) NULL DEFAULT 1,
  `answer_format` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'small_image',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for quiz_results
-- ----------------------------
DROP TABLE IF EXISTS `quiz_results`;
CREATE TABLE `quiz_results`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NULL DEFAULT NULL,
  `result_title` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `image_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `min_correct_count` mediumint(9) NULL DEFAULT NULL,
  `max_correct_count` mediumint(9) NULL DEFAULT NULL,
  `result_order` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for reactions
-- ----------------------------
DROP TABLE IF EXISTS `reactions`;
CREATE TABLE `reactions`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NULL DEFAULT NULL,
  `re_like` int(11) NULL DEFAULT 0,
  `re_dislike` int(11) NULL DEFAULT 0,
  `re_love` int(11) NULL DEFAULT 0,
  `re_funny` int(11) NULL DEFAULT 0,
  `re_angry` int(11) NULL DEFAULT 0,
  `re_sad` int(11) NULL DEFAULT 0,
  `re_wow` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for reading_lists
-- ----------------------------
DROP TABLE IF EXISTS `reading_lists`;
CREATE TABLE `reading_lists`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for roles_permissions
-- ----------------------------
DROP TABLE IF EXISTS `roles_permissions`;
CREATE TABLE `roles_permissions`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `role_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `admin_panel` tinyint(1) NULL DEFAULT NULL,
  `add_post` tinyint(1) NULL DEFAULT NULL,
  `manage_all_posts` tinyint(1) NULL DEFAULT NULL,
  `navigation` tinyint(1) NULL DEFAULT NULL,
  `pages` tinyint(1) NULL DEFAULT NULL,
  `rss_feeds` tinyint(1) NULL DEFAULT NULL,
  `categories` tinyint(1) NULL DEFAULT NULL,
  `widgets` tinyint(1) NULL DEFAULT NULL,
  `polls` tinyint(1) NULL DEFAULT NULL,
  `gallery` tinyint(1) NULL DEFAULT NULL,
  `comments_contact` tinyint(1) NULL DEFAULT NULL,
  `newsletter` tinyint(1) NULL DEFAULT NULL,
  `ad_spaces` tinyint(1) NULL DEFAULT NULL,
  `users` tinyint(1) NULL DEFAULT NULL,
  `seo_tools` tinyint(1) NULL DEFAULT NULL,
  `settings` tinyint(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of roles_permissions
-- ----------------------------
INSERT INTO `roles_permissions` VALUES (1, 'admin', 'Admin', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
INSERT INTO `roles_permissions` VALUES (2, 'moderator', 'Moderator', 1, 1, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0);
INSERT INTO `roles_permissions` VALUES (3, 'author', 'Author', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `roles_permissions` VALUES (4, 'user', 'User', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- ----------------------------
-- Table structure for routes
-- ----------------------------
DROP TABLE IF EXISTS `routes`;
CREATE TABLE `routes`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'admin',
  `profile` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'profile',
  `tag` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'tag',
  `reading_list` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'reading-list',
  `settings` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'settings',
  `social_accounts` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'social-accounts',
  `preferences` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'preferences',
  `visual_settings` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'visual-settings',
  `change_password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'change-password',
  `forgot_password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'forgot-password',
  `reset_password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'reset-password',
  `register` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'register',
  `posts` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'posts',
  `search` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'search',
  `rss_feeds` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'rss-feeds',
  `gallery_album` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'gallery-album',
  `logout` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'logout',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of routes
-- ----------------------------
INSERT INTO `routes` VALUES (1, 'admin', 'profile', 'tag', 'reading-list', 'settings', 'social-accounts', 'preferences', 'visual-settings', 'change-password', 'forgot-password', 'reset-password', 'register', 'posts', 'search', 'rss-feeds', 'gallery-album', 'logout');

-- ----------------------------
-- Table structure for rss_feeds
-- ----------------------------
DROP TABLE IF EXISTS `rss_feeds`;
CREATE TABLE `rss_feeds`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_id` int(11) NULL DEFAULT 1,
  `feed_name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `feed_url` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `post_limit` smallint(6) NULL DEFAULT NULL,
  `category_id` int(11) NULL DEFAULT NULL,
  `image_saving_method` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'url',
  `auto_update` tinyint(1) NULL DEFAULT 1,
  `read_more_button` tinyint(1) NULL DEFAULT 1,
  `read_more_button_text` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'Read More',
  `user_id` int(11) NULL DEFAULT NULL,
  `add_posts_as_draft` tinyint(1) NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for settings
-- ----------------------------
DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_id` int(11) NOT NULL DEFAULT 1,
  `site_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `home_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'Index',
  `site_description` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `keywords` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `application_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `primary_font` smallint(6) NULL DEFAULT 1,
  `secondary_font` smallint(6) NULL DEFAULT 2,
  `tertiary_font` smallint(6) NULL DEFAULT 31,
  `facebook_url` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `twitter_url` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `instagram_url` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pinterest_url` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `linkedin_url` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `vk_url` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `telegram_url` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `youtube_url` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `optional_url_button_name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'Click Here To See More',
  `about_footer` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `contact_text` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `contact_address` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `contact_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `contact_phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `copyright` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cookies_warning` tinyint(1) NULL DEFAULT 0,
  `cookies_warning_text` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of settings
-- ----------------------------
INSERT INTO `settings` VALUES (1, 1, 'SMAN 3 Banjar', 'Index', 'SMAN 3 Banjar Index Page', 'index, home, SMAN 3 Banjar', 'SMAN 3 Banjar', 19, 25, 32, '', '', '', '', '', '', '', '', 'Click Here To See More', '', '', '', '', '', 'Copyright © 2022 SMAN 3 Banjar - All Rights Reserved.', 0, '<p>This site uses cookies. By continuing to browse the site you are agreeing to our use of cookies.</p>');
INSERT INTO `settings` VALUES (2, 2, 'SMAN 3 Banjar', 'Index', 'SMAN 3 Banjar', 'SMAN 3 Banjar, Berita, Kegiatan', 'SMAN18', 19, 25, 32, '', '', '', '', '', '', '', '', 'Click Here To See More', '', '', '', '', '', 'Copyright © 2022 SMAN 3 Banjar - All Rights Reserved.', 0, '');

-- ----------------------------
-- Table structure for subscribers
-- ----------------------------
DROP TABLE IF EXISTS `subscribers`;
CREATE TABLE `subscribers`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `token` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tags
-- ----------------------------
DROP TABLE IF EXISTS `tags`;
CREATE TABLE `tags`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NULL DEFAULT NULL,
  `tag` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tag_slug` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_post_id`(`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'name@domain.com',
  `email_status` tinyint(1) NULL DEFAULT 0,
  `token` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `role` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'user',
  `user_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'registered',
  `google_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `facebook_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `vk_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT 1,
  `about_me` varchar(5000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `facebook_url` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `twitter_url` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `instagram_url` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pinterest_url` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `linkedin_url` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `vk_url` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `telegram_url` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `youtube_url` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `last_seen` timestamp(0) NULL DEFAULT NULL,
  `show_email_on_profile` tinyint(1) NULL DEFAULT 1,
  `show_rss_feeds` tinyint(1) NULL DEFAULT 1,
  `site_mode` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `site_color` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'adminsma', 'adminsma', 'irfan.rona95@student.upi.edu', 1, '6021021adf5f83-70218036-98750114', '$2a$08$FSoljnLht9MAmZeX1finjukv.35Jh.ynYddEBHaYwscibUBhws8ry', 'admin', 'registered', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-14 16:21:06', 1, 1, 'light', 'yellow', '2021-02-08 16:19:22');
INSERT INTO `users` VALUES (2, 'irfanrona', 'irfanrona', 'irfanrona3@gmail.com', 1, '60210d27e247c7-16861815-31220392', '$2a$08$FSoljnLht9MAmZeX1finjukv.35Jh.ynYddEBHaYwscibUBhws8ry', 'moderator', 'registered', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-24 20:26:22', 1, 1, NULL, NULL, '2021-02-08 17:06:31');

-- ----------------------------
-- Table structure for videos
-- ----------------------------
DROP TABLE IF EXISTS `videos`;
CREATE TABLE `videos`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `video_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `video_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for visual_settings
-- ----------------------------
DROP TABLE IF EXISTS `visual_settings`;
CREATE TABLE `visual_settings`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dark_mode` tinyint(1) NOT NULL DEFAULT 0,
  `post_list_style` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'vertical',
  `site_color` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'default',
  `site_block_color` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `logo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `logo_footer` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `logo_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `favicon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of visual_settings
-- ----------------------------
INSERT INTO `visual_settings` VALUES (1, 0, 'horizontal', 'yellow', '#161616', 'uploads/logo/logo_61b060566841c.png', 'uploads/logo/logo_61b060566841c1.png', 'uploads/logo/logo_61b060566841c2.png', 'uploads/logo/logo_61b060566841c3.png');

-- ----------------------------
-- Table structure for widgets
-- ----------------------------
DROP TABLE IF EXISTS `widgets`;
CREATE TABLE `widgets`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_id` int(11) NULL DEFAULT 1,
  `title` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `widget_order` int(11) NULL DEFAULT 1,
  `visibility` int(11) NULL DEFAULT 1,
  `is_custom` int(11) NULL DEFAULT 1,
  `created_at` timestamp(0) NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of widgets
-- ----------------------------
INSERT INTO `widgets` VALUES (1, 1, 'Follow Us', NULL, 'follow-us', 2, 1, 0, '2020-02-18 19:54:39');
INSERT INTO `widgets` VALUES (2, 1, 'Popular Posts', NULL, 'popular-posts', 1, 1, 0, '2020-02-18 19:54:39');
INSERT INTO `widgets` VALUES (3, 1, 'Recommended Posts', NULL, 'recommended-posts', 3, 1, 0, '2020-02-18 19:54:39');
INSERT INTO `widgets` VALUES (4, 1, 'Random Posts', NULL, 'random-slider-posts', 4, 1, 0, '2020-02-18 19:54:39');
INSERT INTO `widgets` VALUES (5, 1, 'Tags', NULL, 'tags', 5, 1, 0, '2020-02-18 19:54:39');
INSERT INTO `widgets` VALUES (6, 1, 'Voting Poll', NULL, 'poll', 6, 1, 0, '2020-02-18 19:54:39');
INSERT INTO `widgets` VALUES (7, 2, 'Ikuti Kami', '', 'follow-us', 1, 1, 0, '2021-02-08 16:24:10');
INSERT INTO `widgets` VALUES (8, 2, 'Berita Populer', '', 'popular-posts', 2, 1, 0, '2021-02-08 16:24:10');
INSERT INTO `widgets` VALUES (9, 2, 'Berita Rekomendasi', '', 'recommended-posts', 3, 1, 0, '2021-02-08 16:24:10');
INSERT INTO `widgets` VALUES (10, 2, 'Berita Acak', '', 'random-slider-posts', 4, 1, 0, '2021-02-08 16:24:10');
INSERT INTO `widgets` VALUES (11, 2, 'Tags', '', 'tags', 5, 1, 0, '2021-02-08 16:24:10');
INSERT INTO `widgets` VALUES (12, 2, 'Voting', '', 'poll', 6, 1, 0, '2021-02-08 16:24:10');

SET FOREIGN_KEY_CHECKS = 1;
