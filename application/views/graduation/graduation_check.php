<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!-- Section: wrapper -->
<div id="wrapper">
    <div class="container m-b-30">
        <div class="row">

            <!--Check breadcrumb active-->
            <?php if ($page->breadcrumb_active == 1): ?>
                <div class="col-sm-12 page-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo lang_base_url(); ?>"><?php echo trans("breadcrumb_home"); ?></a>
                        </li>

                        <li class="breadcrumb-item active"><?php echo html_escape($page->title); ?></li>
                    </ol>
                </div>
            <?php else: ?>
                <div class="col-sm-12 page-breadcrumb"></div>
            <?php endif; ?>

            <div id="content" class="col-sm-12 m-b-30">

                <div class="row">
                    <!--Check page title active-->
                    <?php if ($page->title_active == 1): ?>
                        <div class="col-sm-12">
                            <h1 class="page-title"><?php echo html_escape($page->title); ?></h1>
                        </div>
                    <?php endif; ?>

                    <div class="col-sm-12">
                        <div class="page-contact">

                            <div class="row">
                                <div class="col-sm-12 font-text text-center">
                                    <h2>Pengumuman Kelulusan</h2>
                                    <h3>
                                        SMA Negeri 3 Banjar
                                    </h3>
                                </div>
                            </div>

                            <div class="row row-contact-text">
                                <div class="col-sm-12 font-text text-center">
                                    Masukan 10 Digit Nomor Induk Siswa Nasional (NISN) pada kolom yang telah disediakan.
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-4 col-xs-2"></div>
                                <div class="col-sm-4 col-xs-8">
                                    <!-- include message block -->
                                    <?php $this->load->view('partials/_messages'); ?>

                                    <!-- form start -->
                                    <?php echo form_open('graduation-check'); ?>
                                    <div class="form-group">
                                        <input type="text" class="form-control form-input" id="nisn" name="nisn" placeholder="Masukan NISN" maxlength="10" minlength="10" <?php echo ($this->rtl == true) ? 'dir="rtl"' : ''; ?> required>
                                    </div>
                                    
                                    <?php generate_recaptcha(); ?>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-md btn-custom pull-right">
                                            Lihat Hasil
                                        </button>
                                    </div>

                                    </form><!-- form end -->


                                </div>

                                <div class="col-sm-4 col-xs-2"></div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
<!-- /.Section: wrapper -->
<style>
    #footer {
        margin-top: 0;
    }
</style>
<script>
    var iframe = document.getElementById("contact_iframe");
    if (iframe) {
        iframe.src = iframe.src;
        iframe.src = iframe.src;
    }

    // Get a reference to the input element
    var nisnInput = document.getElementById('nisn');

    // Add an event listener to the form submit event
    nisnInput.form.addEventListener('submit', function(event) {
        // Check if the input value has exactly 10 characters
        if (nisnInput.value.length !== 10) {
            // If not, set a custom validation message
            nisnInput.setCustomValidity('NISN must be exactly 10 characters.');
        } else {
            // Clear any custom validation message
            nisnInput.setCustomValidity('');
        }
    });
</script>