<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!-- Section: wrapper -->
<div id="wrapper">
    <div class="container m-b-30">
        <div class="row">

            <!--Check breadcrumb active-->
            <?php if ($page->breadcrumb_active == 1): ?>
                <div class="col-sm-12 page-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo lang_base_url(); ?>"><?php echo trans("breadcrumb_home"); ?></a>
                        </li>

                        <li class="breadcrumb-item active"><?php echo html_escape($page->title); ?></li>
                    </ol>
                </div>
            <?php else: ?>
                <div class="col-sm-12 page-breadcrumb"></div>
            <?php endif; ?>

            <div id="content" class="col-sm-12 m-b-30">

                <div class="row">
                    <!--Check page title active-->
                    <?php if ($page->title_active == 1): ?>
                        <div class="col-sm-12">
                            <h1 class="page-title"><?php echo html_escape($page->title); ?></h1>
                        </div>
                    <?php endif; ?>

                    <div class="col-sm-12">
                        <div class="team-boxed">
                            <div class="container">
                            <div class="row people">
                                    <div class="col-md-12 col-lg-10 col-lg-offset-1 item">
                                        <div class="box">
                                            <!-- <img class="rounded-circle" src="assets/img/1.jpg"> -->
                                            <h3 class="name">Irfan Haidar Rachman</h3>
                                            <p class="title">XII IPS 2</p>
                                            <!-- <p class="description">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est, et interdum justo suscipit id. Etiam dictum feugiat tellus, a semper massa. </p> -->
                                            <div class="social">
                                                <div class="col-md-4">
                                                    <p><i class="fa fa-database"></i> 0123456789 </p>
                                                </div>
                                                <div class="col-md-4">
                                                    <p><i class="fa fa-calendar"></i> 14 Juli 1995 </p>
                                                </div>
                                                <div class="col-md-4">
                                                    <p><i class="fa fa-venus-mars"></i> Perempuan</p>
                                                </div>
                                            </div>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                                <div class="intro">
                                    <p class="text-center">Selamat anda dinyatakan</p>
                                    <h2 class="text-center">LULUS</h2>
                                </div>
                                <!-- <div class="row people">
                                    <div class="col-md-12 col-lg-12 item">
                                        <div class="box"><img class="rounded-circle" src="assets/img/1.jpg">
                                            <h3 class="name">Ben Johnson</h3>
                                            <p class="title">Musician</p>
                                            <p class="description">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est, et interdum justo suscipit id. Etiam dictum feugiat tellus, a semper massa. </p>
                                            <div class="social"><a href="#"><i class="fa fa-facebook-official"></i></a><a href="#"><i class="fa fa-twitter"></i></a><a href="#"><i class="fa fa-instagram"></i></a></div>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.Section: wrapper -->
<style>
    #footer {
        margin-top: 0;
    }
</style>
<script>
    var iframe = document.getElementById("contact_iframe");
    if (iframe) {
        iframe.src = iframe.src;
        iframe.src = iframe.src;
    }

    // Get a reference to the input element
    var nisnInput = document.getElementById('nisn');

    // Add an event listener to the form submit event
    nisnInput.form.addEventListener('submit', function(event) {
        // Check if the input value has exactly 10 characters
        if (nisnInput.value.length !== 10) {
            // If not, set a custom validation message
            nisnInput.setCustomValidity('NISN must be exactly 10 characters.');
        } else {
            // Clear any custom validation message
            nisnInput.setCustomValidity('');
        }
    });
</script>